import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "./App.css";
import Menu from "./Utils/Menu";

function Header() {
  const login = useSelector((s) => s.login);

  return (
    <header className="headerContainer">
      <div>
        <Link to="/">
          <div className="logo" />
        </Link>
      </div>
      <div className="linksContainer">
        <button
          //className="burgerUser"
          style={{
            fontSize: "16px",
            borderRadius: "15px",
            padding: "2px",
            marginRight: "20px",
            fontWeight: "bold",
            boxShadow: "0px 4px 2px -2px rgba(0, 0, 0, 0.75)",
          }}
        >
          <Link to="/preguntas">¿Dudas?</Link>
        </button>
        <div>{!login && <Link to="/login">Iniciar sesión</Link>}</div>
        <div>
          {login && (
            <div className="userMenu">
              <div
                className="avatar"
                style={
                  login.imagen && {
                    backgroundImage: `url(http://localhost:9999/images/${login.imagen}.jpg)`,
                  }
                }
              />
              <Menu>{login.username}</Menu>
            </div>
          )}
        </div>
        <div>{!login && <Link to="/register">Registro</Link>}</div>
      </div>
    </header>
  );
}

export default Header;
