import React from "react";
import { Link } from "react-router-dom";
import "./footer.css";
import instagram from "./iconsFooter/instagram.png";
import linkedin from "./iconsFooter/linkedin.png";
import facebook from "./iconsFooter/facebook.png";
import twitter from "./iconsFooter/twitter.png";

export const Footer = (props) => {
  return (
    <div className="footerContainer">
      <div className="footerRight">
        <a href="https://www.facebook.com/">
          <img src={facebook} className="iconoYou" alt="Canvas Logo" />
        </a>
        <a href="https://www.instagram.com/">
          <img src={instagram} className="iconoYou" alt="Canvas Logo" />
        </a>
        <a href="https://www.twitter.com/">
          <img src={twitter} className="iconoYou" alt="Canvas Logo" />
        </a>
        <a href="https://www.linkedin.com/">
          <img src={linkedin} className="iconoYou" alt="Canvas Logo" />
        </a>

        <div>
          <a target="_blank" rel="noreferrer" href="https://www.google.com">
            Contacta
          </a>
        </div>
      </div>
    </div>
  );
};
